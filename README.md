# Lyra Presidential App

A critical web app for tracking elections results

### Prerequisites

* PHP 7
* MySQL
 

## Built With

* [Symfony](https://www.symfony.com/) - Symfony Framework
* [Admin BSB Material Design Admin](https://github.com/gurayyarar/AdminBSBMaterialDesign) - AdminBSB Material Design Administration Dashboard Template
* [ChartJS](https://www.chartjs.org/) - ChartJS

## License
<a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-nd/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/4.0/">Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License</a>.


